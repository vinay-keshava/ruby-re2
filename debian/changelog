ruby-re2 (1.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.6.0

 -- Vinay Keshava <vinaykeshava@disroot.org>  Sat, 29 Oct 2022 18:12:43 +0530

ruby-re2 (1.4.0-1) unstable; urgency=medium

  * Team Upload

  [ Sruthi Chandran ]
  * Update minimum version of gem2deb to 1.0

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.

  [ Pirate Praveen ]
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * New upstream version 1.4.0

 -- Pirate Praveen <praveen@debian.org>  Sat, 30 Jul 2022 12:41:52 +0200

ruby-re2 (1.2.0-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * New upstream release.
  * Bump copyright years.
  * Bump debhelper compat to 12.
  * Declare Rules-Requires-Root: no.
  * Update VCS URLs to salsa.debian.org.
  * Use https URLs in package metadata.
  * Bump Standards-Version to 4.5.0, no changes needed.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Stefano Rivera <stefanor@debian.org>  Mon, 11 May 2020 16:26:18 -0700

ruby-re2 (1.1.1-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable
  * Bump standards version to 4.1.3

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Feb 2018 20:00:45 +0530

ruby-re2 (1.1.1-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * Bump standards version to 4.0.0
  * Drop patch: c++11 (applied upstream)
  * Add patch: require-spec-helper.patch (fix tests)

 -- Pirate Praveen <praveen@debian.org>  Sat, 19 Aug 2017 11:04:33 +0530

ruby-re2 (0.7.0-2) unstable; urgency=medium

  * Team upload.
  * Build with --std=c++11. RE2 now requires this. (Closes: #820349)
  * Bump Standards-Version to 3.9.8, no changes needed.

  [ Cédric Boutillier ]
  * Switch VCS fields to use https.
  * Bump compat level to 9.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 08 May 2016 20:03:48 +0200

ruby-re2 (0.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Refresh packaging:
    + Bump Standards-Version to 3.9.6
    + Update Vcs-Browser to use cgit
    + Add Testsuite field
    + Use rake-based test runner

 -- Lucas Nussbaum <lucas@debian.org>  Wed, 06 May 2015 21:39:05 +0200

ruby-re2 (0.6.0-1) unstable; urgency=medium

  * New upstream version: fix FTBFS (Closes: #748118)
  * Upload to unstable
  * Bump copyright years

 -- David Suárez <david.sephirot@gmail.com>  Wed, 04 Jun 2014 20:12:06 +0200

ruby-re2 (0.5.0-1) experimental; urgency=medium

  * Initial release (Closes: #735672)

 -- David Suárez <david.sephirot@gmail.com>  Sun, 26 Jan 2014 17:08:13 +0100
